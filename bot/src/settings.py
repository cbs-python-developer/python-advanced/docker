from dataclasses import dataclass
from typing import Optional

from dotenv import load_dotenv
import os


load_dotenv()


@dataclass
class Settings:
    BOT_NAME: Optional[str] = os.environ.get("BOT_NAME")
    BOT_TOKEN: Optional[str] = os.environ.get("BOT_TOKEN")
    BOT_USERNAME: Optional[str] = os.environ.get("BOT_USERNAME")

    POSTGRES_NAME: Optional[str] = os.environ.get("POSTGRES_DB")
    POSTGRES_USER: Optional[str] = os.environ.get("POSTGRES_USER")
    POSTGRES_PASSWORD: Optional[str] = os.environ.get("POSTGRES_PASSWORD")
    POSTGRES_HOST: Optional[str] = os.environ.get("POSTGRES_HOST")
    POSTGRES_PORT_HOST: Optional[str] = os.environ.get("POSTGRES_PORT_HOST")


settings = Settings()
