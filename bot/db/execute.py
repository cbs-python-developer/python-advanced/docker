from datetime import datetime
from typing import Optional
from zoneinfo import ZoneInfo

from psycopg import OperationalError, connection

from bot.db.connection import conn


def execute_query(
        query: str,
        data: Optional[dict] = None,
        db_conn: connection = conn
) -> None:
    """Execute the SQL query."""
    cursor = db_conn.cursor()
    try:
        cursor.execute(query, data)
        db_conn.commit()
        print(f"Query executed successfully")
    except OperationalError as e:
        print(f"Error: {e}")


def insert_record(message_text: str, user_id: int) -> None:
    """Insert a new record into the database."""
    insert_query = f"""    
        INSERT INTO message (message_text, user_id, message_timestamp)
        VALUES (%(message_text)s, %(user_id)s, %(timestamp_now)s)
    """
    insert_data = {
        "message_text": message_text,
        "user_id": user_id,
        'timestamp_now': datetime.now(tz=ZoneInfo("UTC")).isoformat(sep=" ")
    }
    execute_query(insert_query, insert_data)


if __name__ == "__main__":

    create_table_query = """
    CREATE TABLE IF NOT EXISTS message (
        id SERIAL PRIMARY KEY,
        message_text TEXT,
        user_id BIGINT NOT NULL,
        message_timestamp TIMESTAMP WITH TIME ZONE NOT NULL
    );
    """

    execute_query(create_table_query)
