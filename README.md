# Python Advanced UA

# Python + Docker

Що ми отримаємо в результаті:
1. Загальне розуміння використання docker: для чого і як?
2. Встановимо всі необхідні інструменти для використання docker і docker-compose
3. Створимо тренувальний python-проект, який будемо запускати в docker-контейнері
4. Розширимо проект для роботи із базою даних:
    1. Загальне розуміння використання docker-compose: для чого і як?
    2. Розгорнемо сервер бази даних PostgreSQL в іншому контейнері і навчимось запускати все це так, щоб наш
застосунок працював з БД.
5. Фіналізуємо проект – додамо клієнт PgAdmin для зручної роботи з БД на етапі розробки в додатковому
контейнері.

Побудована нами структура буде носити загальний характер і може бути використана як основа для інших python-проектів які будуть використовувати docker-контейнерізацю.